from django.apps import AppConfig
from django.contrib.contenttypes.models import ContentType

from vault.models import Person, Contact, BankAcct, BankTransaction, Frequency, BankAccountSubscription, \
    CommissionRateTransaction
from vault.settings import TYPE_MEMBER, TYPE_VAULT, TYPE_DEALER
from decimal import Decimal

import datetime


class ManagerConfig(AppConfig):
    name = 'admin'
    label = 'vault_admin'


class PersonController(object):
    def get_active_choice(self):
        contact = Contact.objects.get(Ctype=TYPE_MEMBER)
        persons = Person.objects.filter(PStatus=Person.ACTIVO, contact=contact)
        choice = []
        for person in persons:
            choice.append((person.id, person.full_name()))
        return choice

    @staticmethod
    def available_transfer(member_id=None, member=None):

        if not member:
            try:
                member = Person.objects.get(id=member_id)
            except Person.DoesNotExist:
                raise Exception('Failed in get data of Member')

        # All transfer
        # content_type = ContentType.objects.get_for_model(member)
        # BankTransaction.objects.filter(content_type=content_type, object_id=member.id,
        #                                                  TransContactType=TYPE_MEMBER)
        all_transactions = member.transactions.all()

        sum_deposits = 0
        sum_transfer_debit = 0
        sum_transfer_credit = 0
        limit = 0

        for transfer in all_transactions:
            # TODO debe incluir los depósitos por transacción?
            if transfer.TransTypeOperation == BankTransaction.TO_DEPOSIT and transfer.TransStatus == BankTransaction.STATUS_PROCESSED:
                sum_deposits += transfer.TransCredit

            # TODO se incluye solo transferencia a mi banco
            if transfer.TransTypeOperation == BankTransaction.TO_MY_BANK:
                if transfer.TransStatus == BankTransaction.STATUS_PROCESSED or transfer.TransStatus == BankTransaction.STATUS_PENDING:
                    sum_transfer_debit += transfer.TransDebit

            # TODO se incluye solo transferencia a mi banco
            if transfer.TransTypeOperation == BankTransaction.TO_MEMBER:
                if transfer.TransStatus == BankTransaction.STATUS_PROCESSED or transfer.TransStatus == BankTransaction.STATUS_PENDING:
                    sum_transfer_debit += transfer.TransDebit
                    sum_transfer_credit += transfer.TransCredit
        limit = (sum_deposits + sum_transfer_credit) * Decimal('0.9') - sum_transfer_debit
        return limit

    @staticmethod
    def available_fund(member=None, member_id=None):

        if not member:
            try:
                member = Person.objects.get(id=member_id)
            except Person.DoesNotExist:
                raise Exception('Failed in get data of Member')

        all_transactions = member.transactions.all()

        sum_deposits = 0
        sum_commission = 0
        sum_transfer_debit = 0

        for transfer in all_transactions:

            if transfer.TransTypeOperation == BankTransaction.TO_DEPOSIT and transfer.TransStatus == BankTransaction.STATUS_PROCESSED:
                sum_deposits += transfer.TransCredit

            if (transfer.TransTypeOperation == BankTransaction.TO_STORE_PAYMENT or
                        transfer.TransTypeOperation == BankTransaction.TO_STORE_TRANSFER or
                        transfer.TransTypeOperation == BankTransaction.TO_MY_BANK or
                        transfer.TransTypeOperation == BankTransaction.TO_MEMBER):

                if (transfer.TransStatus == BankTransaction.STATUS_PROCESSED or
                            transfer.TransStatus == BankTransaction.STATUS_PENDING):
                    sum_transfer_debit += transfer.TransDebit

        return sum_deposits - sum_commission - sum_transfer_debit

    @staticmethod
    def accounting_fund(member=None, member_id=None):

        if not member:
            try:
                member = Person.objects.get(id=member_id)
            except Person.DoesNotExist:
                raise Exception('Failed in get data of Member')

        all_transactions = member.transactions.all()
        sum_deposits = 0
        sum_transfer_debit = 0

        for transfer in all_transactions:

            if transfer.TransTypeOperation == BankTransaction.TO_DEPOSIT and transfer.TransStatus == BankTransaction.STATUS_PROCESSED:
                sum_deposits += transfer.TransCredit

            if (transfer.TransTypeOperation == BankTransaction.TO_STORE_PAYMENT or
                        transfer.TransTypeOperation == BankTransaction.TO_STORE_TRANSFER or
                        transfer.TransTypeOperation == BankTransaction.TO_MY_BANK or
                        transfer.TransTypeOperation == BankTransaction.TO_MEMBER):
                if transfer.TransStatus == BankTransaction.STATUS_PROCESSED:
                    sum_transfer_debit += transfer.TransDebit

        return sum_deposits - sum_transfer_debit


class TransferController(object):
    @staticmethod
    def create(params):

        type = params['type']
        amount = Decimal(params['amount'])
        credit = 0
        debit = 0
        from_bank_id = None
        to_bank_id = None
        frequency_id = None
        subscription_id = None
        vault = Contact.objects.get(Ctype=TYPE_VAULT)
        mirror_from_bank_id = None
        to_contact_id = None
        to_person_id = None
        from_person_id = None
        status = BankTransaction.STATUS_PENDING
        flg_bar_code = BankTransaction.WITHOUT_CODE
        subtotal = amount
        amount_commission = 0
        transfer = amount
        try:
            member = Person.objects.get(id=params['member_id'])
            commission = CommissionRateTransaction.objects.get(TransTypeOperation=type, agentType=CommissionRateTransaction.MEMBER_AGENT)
            amount_commission = commission.Ammount
        except CommissionRateTransaction.DoesNotExist:
            amount_commission = 0

        if type == BankTransaction.TO_DEPOSIT:
            subscription = params['subscription']
            from_bank_id = subscription.bank_id
            to_bank_id = BankAcct.VAULT_BANK_ID
            to_contact_id = BankAcct.VAULT_BANK_ID
            frequency_id = subscription.Frequency_id
            subscription_id = subscription.id
            mirror_from_bank_id = BankAcct.VAULT_BANK_ID
            credit = amount - amount_commission

        if type == BankTransaction.TO_MEMBER:
            to_person_id = params['to_member_id']
            debit = amount + amount_commission
            transfer = 0
            status = BankTransaction.STATUS_PROCESSED

        if type == BankTransaction.TO_STORE_PAYMENT or type == BankTransaction.TO_STORE_TRANSFER:
            to_bank = BankAcct.objects.get(id=params['to_bank_id'])
            to_contact_id = to_bank.object_id
            transfer = (amount - amount_commission) if type == BankTransaction.TO_STORE_PAYMENT else amount
            debit = amount if type == BankTransaction.TO_STORE_PAYMENT else amount + amount_commission
            amount_commission = amount_commission if type == BankTransaction.TO_STORE_TRANSFER else 0
            flg_bar_code = BankTransaction.WITH_CODE
            status = BankTransaction.STATUS_PRE_PENDING

        if type == BankTransaction.TO_MY_BANK:
            #from_bank_id = BankAcct.VAULT_BANK_ID
            to_bank_id = params['to_bank_id']
            to_person_id = member.id
            debit = amount + amount_commission

        if type == BankTransaction.TO_DEPOSIT:
            trans_ref = BankTransaction.TYPE_DEPOSIT
        else:
            trans_ref = BankTransaction.TYPE_PURCHASE

        data = {
            'user': member,
            'trans_ref': trans_ref,
            'from_bank_id': from_bank_id,
            'credit': credit,
            'debit': debit,
            'to_bank_id': to_bank_id,
            'frequency_id': frequency_id,
            'subscription_id': subscription_id,
            'type_operation': type,
            'balance_fund': None,
            'vault': vault,
            'mirror_from_bank_id': mirror_from_bank_id,
            'to_contact_id': to_contact_id,
            'to_person_id': to_person_id,
            'status': status,
            'flg_bar_code': flg_bar_code,
            'flg_commission': 0,
            'subtotal': subtotal,
            'commission': amount_commission,
            'transfer': transfer,
            'from_person_id': from_person_id,
            'commentary': params['commentary']
        }

        transaction = TransferController().create_transaction(data)
        member.Fund = member.Fund if member.Fund is not None else 0
        transaction.BeforeFund = member.Fund
        member.Fund = PersonController().available_fund(member=member)
        member.CountableBalance = PersonController().accounting_fund(member=member)
        transaction.BalanceFund = member.Fund
        data['transfer'] = 0

        if type == BankTransaction.TO_MEMBER:
            transaction.ResponseDate = datetime.datetime.now()

            to_member = Person.objects.get(id=params['to_member_id'])
            credit = data['subtotal']
            debit = 0
            data['credit'] = credit
            data['debit'] = debit
            data['subtotal'] = data['subtotal']
            data['user'] = to_member
            data['to_person_id'] = None
            data['type_operation'] = BankTransaction.TO_DEPOSIT
            data['flg_commission'] = 0
            data['commission'] = 0
            data['from_person_id'] = member.id

            to_transaction = TransferController().create_transaction(params=data, offset_tran_id=transaction.id)
            to_member.Fund = to_member.Fund if to_member.Fund is not None else 0
            to_transaction.BeforeFund = to_member.Fund

            to_member.Fund = PersonController().available_fund(member=to_member)
            to_member.CountableBalance = PersonController().accounting_fund(member=to_member)
            to_member.save()

            to_transaction.BalanceFund = to_member.Fund
            to_transaction.ResponseDate = datetime.datetime.now()
            to_transaction.save()

        # se comenta porque dave pidio que cuando se realize un pago desde el cliente no se actualizen los montos aun
        if type == BankTransaction.TO_STORE_PAYMENT or type == BankTransaction.TO_STORE_TRANSFER:
            dealer = Contact.objects.get(id=data['to_contact_id'])
            data['user'] = dealer
            data['commission'] = commission.Ammount if type == BankTransaction.TO_STORE_PAYMENT else 0
            data['to_bank_id'] = to_bank_id
            data['credit'] = data['subtotal'] if type == BankTransaction.TO_STORE_TRANSFER else (
            Decimal(data['subtotal']) - data['commission'])
            data['debit'] = 0
            data['flg_bar_code'] = BankTransaction.WITHOUT_CODE
            data['from_bank_id'] = None
            data['to_contact_id'] = None
            data['to_person_id'] = params['member_id']

            transaction2 = TransferController().create_transaction(params=data, offset_tran_id=transaction.id)
            dealer.Fund = dealer.Fund if dealer.Fund is not None else 0
            transaction2.BeforeFund = dealer.Fund
            dealer.Fund = ContactController().available_fund(contact=dealer)
            dealer.CountableBalance = ContactController().accounting_fund(contact=dealer)
            transaction2.BalanceFund = dealer.Fund
            transaction2.save()
            dealer.save()

        transaction.save()
        member.save()

        # TransferController.transaction_to_commission(transaction)

        return transaction


    @staticmethod
    def createForDealer(params):

        type = params['type']
        amount = Decimal(params['amount'])
        credit = 0
        debit = 0
        from_bank_id = None
        to_bank_id = None
        frequency_id = None
        subscription_id = None
        vault = Contact.objects.get(Ctype=TYPE_VAULT)
        mirror_from_bank_id = None
        to_contact_id = None
        to_person_id = None
        from_person_id = None
        from_contact_id = None
        status = BankTransaction.STATUS_PENDING
        flg_bar_code = BankTransaction.WITHOUT_CODE
        subtotal = amount
        amount_commission = 0
        transfer = amount
        try:
            dealer = Contact.objects.get(id=params['dealer_id'])        
            commission = CommissionRateTransaction.objects.get(TransTypeOperation=type, agentType=CommissionRateTransaction.DEALER_AGENT, classification=dealer.classification)
            amount_commission = commission.Ammount
        except CommissionRateTransaction.DoesNotExist:
            amount_commission = 0

        """ ESTO SERVIRA CUANDO IMPLEMENTEMOS LA OPCION DE DEPOSITOS PARA DEALERS
        if type == BankTransaction.TO_DEPOSIT:
            subscription = params['subscription']
            from_bank_id = subscription.bank_id
            to_bank_id = BankAcct.VAULT_BANK_ID
            to_contact_id = BankAcct.VAULT_BANK_ID
            frequency_id = subscription.Frequency_id
            subscription_id = subscription.id
            mirror_from_bank_id = BankAcct.VAULT_BANK_ID
            credit = amount - amount_commission
        """

        if type == BankTransaction.TO_STORE:
            to_contact_id = params['to_dealer_id']
            debit = amount + amount_commission
            transfer = 0
            status = BankTransaction.STATUS_PROCESSED
            from_contact_id = dealer.id

        #OPCION PARA DEPOSITOS A MI CUENTA BANCARIA DE DEALER
        if type == BankTransaction.TO_MY_BANK:
            #from_bank_id = BankAcct.VAULT_BANK_ID
            to_bank_id = params['to_bank_id']
            from_contact_id = dealer.id
            debit = amount + amount_commission
        

        if type == BankTransaction.TO_DEPOSIT:
            trans_ref = BankTransaction.TYPE_DEPOSIT
        else:
            trans_ref = BankTransaction.TYPE_PURCHASE

        data = {
            'user': dealer,
            'trans_ref': trans_ref,
            'from_bank_id': from_bank_id,
            'credit': credit,
            'debit': debit,
            'to_bank_id': to_bank_id,
            'frequency_id': frequency_id,
            'subscription_id': subscription_id,
            'type_operation': type,
            'balance_fund': None,
            'vault': vault,
            'mirror_from_bank_id': mirror_from_bank_id,
            'to_contact_id': to_contact_id,
            'to_person_id': to_person_id,
            'status': status,
            'flg_bar_code': flg_bar_code,
            'flg_commission': 0,
            'subtotal': subtotal,
            'commission': amount_commission,
            'transfer': transfer,
            'from_contact_id' : from_contact_id,
            'from_person_id' : from_person_id
        }

        transaction = TransferController().create_transactionForDealer(data)
        transaction.BeforeFund = dealer.Fund
        dealer.Fund = ContactController().available_fund(contact=dealer)
        dealer.CountableBalance = ContactController().accounting_fund(contact=dealer)
        transaction.BalanceFund = dealer.Fund
        data['transfer'] = 0

        if type == BankTransaction.TO_STORE:
            transaction.ResponseDate = datetime.datetime.now()
            to_dealer = Contact.objects.get(id=params['to_dealer_id'])
            credit = data['subtotal']
            debit = 0
            data['credit'] = credit
            data['debit'] = debit
            data['subtotal'] = data['subtotal']
            data['user'] = to_dealer
            data['to_contact_id'] = to_dealer.id
            data['type_operation'] = BankTransaction.TO_DEPOSIT
            data['flg_commission'] = 0
            data['commission'] = 0
            data['from_contact_id'] = dealer.id

            to_transaction = TransferController().create_transactionForDealer(params=data, offset_tran_id=transaction.id)
            to_dealer.Fund = to_dealer.Fund if to_dealer.Fund is not None else 0
            to_transaction.BeforeFund = to_dealer.Fund

            to_dealer.Fund = ContactController().available_fund(contact=to_dealer)
            to_dealer.CountableBalance = ContactController().accounting_fund(contact=to_dealer)
            to_dealer.save()

            to_transaction.BalanceFund = to_dealer.Fund
            to_transaction.ResponseDate = datetime.datetime.now()
            to_transaction.save()

        transaction.save()
        dealer.save()
        return transaction

    @staticmethod
    def createRejected(params):

        type = params['type']
        amount = Decimal(params['amount'])
        credit = 0
        debit = 0
        from_bank_id = None
        to_bank_id = None
        frequency_id = None
        subscription_id = None
        vault = Contact.objects.get(Ctype=TYPE_VAULT)
        mirror_from_bank_id = None
        to_contact_id = None
        to_person_id = None
        # Status Procesado
        status = BankTransaction.STATUS_PROCESSED
        flg_bar_code = BankTransaction.WITHOUT_CODE
        subtotal = amount
        amount_commission = 0
        transfer = amount
        trans_ref = BankTransaction.TYPE_PURCHASE
        from_person_id = None

        # Todas las transacciones que no son el pago de un retiro creado por el dealer
        if params['flg_dealer_transaction'] == 1:
            member = Contact.objects.get(id=params['member_id'])
            debit = params['amount']
        else:
            member = params['content_object']
            credit = params['amount']

        data = {
            'user': member,
            'trans_ref': trans_ref,
            'from_bank_id': from_bank_id,
            'credit': credit,
            'debit': debit,
            'to_bank_id': to_bank_id,
            'frequency_id': frequency_id,
            'subscription_id': subscription_id,
            'type_operation': type,
            'balance_fund': None,
            'vault': vault,
            'mirror_from_bank_id': mirror_from_bank_id,
            'to_contact_id': to_contact_id,
            'to_person_id': to_person_id,
            'status': status,
            'flg_bar_code': flg_bar_code,
            'flg_commission': 0,
            'subtotal': subtotal,
            'commission': amount_commission,
            'transfer': transfer,
            'from_person_id': from_person_id,
            'commentary':' '
        }

        transaction = TransferController().create_transaction(params=data, offset_tran_id=params['offset_tran_id'])
        member.Fund = member.Fund if member.Fund is not None else 0
        if params['flg_dealer_transaction'] == 1:
            # transaction de compensacion para el dealer
            transaction.BeforeFund = member.Fund
            member.Fund = ContactController().available_fund(contact=member)
            member.CountableBalance = ContactController().accounting_fund(contact=member)
            transaction.BalanceFund = member.Fund
            data['transfer'] = 0
            transaction.save()
            member.save()
        else:
            # transaction de compensacion para el cliente
            transaction.BeforeFund = member.Fund
            member.Fund = PersonController().available_fund(member=member)
            member.CountableBalance = PersonController().accounting_fund(member=member)
            transaction.BalanceFund = member.Fund

        return transaction

    @staticmethod
    def create_transaction(params, offset_tran_id=None, trans_dealer=None):

        user = params['user']
        content_type = ContentType.objects.get_for_model(user)
        type_user = TYPE_DEALER if content_type.model == 'contact' else TYPE_MEMBER
        type = params['type_operation']

        transaction_member = BankTransaction()
        transaction_member.content_object = user
        transaction_member.TransRef = params['trans_ref']
        transaction_member.from_bank_id = params['from_bank_id']
        transaction_member.TransCredit = params['credit']
        transaction_member.TransDebit = params['debit']
        transaction_member.trans_to_bank_id = params['to_bank_id']
        transaction_member.TransDate = datetime.datetime.now()
        transaction_member.CreateDate = datetime.datetime.now()
        transaction_member.TransContactType = type_user
        transaction_member.TransFrequencyId = params['frequency_id']
        transaction_member.TransSubscriptionId = params['subscription_id']
        transaction_member.TransTypeOperation = type
        transaction_member.offset_trans_id = offset_tran_id
        transaction_member.TransStatus = params['status']
        transaction_member.to_contact_id = params['to_contact_id']
        transaction_member.to_person_id = params['to_person_id']
        transaction_member.from_person_id = params['from_person_id']
        transaction_member.flgCodigoBarras = params['flg_bar_code']
        transaction_member.flgCommmission = params['flg_commission']
        transaction_member.TransSubtotal = params['subtotal']
        transaction_member.TransCommission = params['commission']
        transaction_member.TransAmountTransfer = params['transfer']
        transaction_member.commentary = params['commentary']
        transaction_member.last_modify_date = datetime.datetime.now()
        transaction_member.save()

        transaction_vault = BankTransaction()
        transaction_vault.content_object = params['vault']
        transaction_vault.TransRef = params['trans_ref']
        transaction_vault.from_bank_id = params['mirror_from_bank_id']
        transaction_vault.TransCredit = params['debit']
        transaction_vault.TransDebit = params['credit']
        transaction_vault.offset_trans = transaction_member
        transaction_vault.TransDate = datetime.datetime.now()
        transaction_vault.CreateDate = datetime.datetime.now()
        transaction_vault.TransContactType = TYPE_VAULT
        transaction_vault.TransFrequencyId = params['frequency_id']
        transaction_vault.TransSubscriptionId = params['subscription_id']
        transaction_vault.TransStatus = params['status']
        transaction_vault.flgCodigoBarras = params['flg_bar_code']
        transaction_vault.TransTypeOperation = type
        transaction_vault.flgCommmission = params['flg_commission']
        transaction_vault.TransSubtotal = None
        transaction_vault.TransCommission = None
        transaction_vault.TransAmountTransfer = None
        transaction_vault.last_modify_date = datetime.datetime.now()
        transaction_vault.save()

        return transaction_member

    @staticmethod
    def create_transactionForDealer(params, offset_tran_id=None, trans_dealer=None):

        user = params['user']
        content_type = ContentType.objects.get_for_model(user)
        type_user = TYPE_DEALER if content_type.model == 'contact' else TYPE_MEMBER
        type = params['type_operation']

        transaction_member = BankTransaction()
        transaction_member.content_object = user
        transaction_member.TransRef = params['trans_ref']
        transaction_member.from_bank_id = params['from_bank_id']
        transaction_member.TransCredit = params['credit']
        transaction_member.TransDebit = params['debit']
        transaction_member.trans_to_bank_id = params['to_bank_id']
        transaction_member.TransDate = datetime.datetime.now()
        transaction_member.CreateDate = datetime.datetime.now()
        transaction_member.TransContactType = type_user
        transaction_member.TransFrequencyId = params['frequency_id']
        transaction_member.TransSubscriptionId = params['subscription_id']
        transaction_member.TransTypeOperation = type
        transaction_member.offset_trans_id = offset_tran_id
        transaction_member.TransStatus = params['status']
        transaction_member.to_contact_id = params['to_contact_id']
        transaction_member.from_contact_id = params['from_contact_id']
        transaction_member.to_person_id = params['to_person_id']
        transaction_member.from_person_id = params['from_person_id']
        transaction_member.flgCodigoBarras = params['flg_bar_code']
        transaction_member.flgCommmission = params['flg_commission']
        transaction_member.TransSubtotal = params['subtotal']
        transaction_member.TransCommission = params['commission']
        transaction_member.TransAmountTransfer = params['transfer']
        transaction_member.last_modify_date = datetime.datetime.now()
        transaction_member.save()

        transaction_vault = BankTransaction()
        transaction_vault.content_object = params['vault']
        transaction_vault.TransRef = params['trans_ref']
        transaction_vault.from_bank_id = params['mirror_from_bank_id']
        transaction_vault.TransCredit = params['debit']
        transaction_vault.TransDebit = params['credit']
        transaction_vault.offset_trans = transaction_member
        transaction_vault.TransDate = datetime.datetime.now()
        transaction_vault.CreateDate = datetime.datetime.now()
        transaction_vault.TransContactType = TYPE_VAULT
        transaction_vault.TransFrequencyId = params['frequency_id']
        transaction_vault.TransSubscriptionId = params['subscription_id']
        transaction_vault.TransStatus = params['status']
        transaction_vault.flgCodigoBarras = params['flg_bar_code']
        transaction_vault.TransTypeOperation = type
        transaction_vault.flgCommmission = params['flg_commission']
        transaction_vault.TransSubtotal = None
        transaction_vault.TransCommission = None
        transaction_vault.TransAmountTransfer = None
        transaction_member.last_modify_date = datetime.datetime.now()
        transaction_vault.save()

        return transaction_member

    @staticmethod
    def transaction_to_commission(trans):

        type = BankTransaction.TO_COMMISSION
        if trans.TransContactType == TYPE_MEMBER:
            user = Person.objects.get(id=trans.object_id)
        else:
            user = Contact.objects.get(id=trans.object_id)

        try:
            commission = CommissionRateTransaction.objects.get(TransTypeOperation=trans.TransTypeOperation, agentType=CommissionRateTransaction.MEMBER_AGENT)
        except CommissionRateTransaction.DoesNotExist:
            raise Exception('Failed in get commission')

        data = {
            'user': user,
            'trans_ref': BankTransaction.TYPE_DEPOSIT if type == BankTransaction.TO_DEPOSIT else BankTransaction.TYPE_PURCHASE,
            'from_bank_id': None,
            'credit': 0,
            'debit': commission.Ammount,
            'to_bank_id': None,
            'frequency_id': None,
            'subscription_id': None,
            'type_operation': BankTransaction.TO_COMMISSION,
            'balance_fund': None,
            'vault': Contact.objects.get(Ctype=TYPE_VAULT),
            'mirror_from_bank_id': None,
            'to_contact_id': None,
            'to_person_id': None,
            'status': BankTransaction.STATUS_PENDING,
            'flg_bar_code': None,
            'flg_commission': 0,
            'commentary':''
        }

        transaction = TransferController().create_transaction(params=data, offset_tran_id=trans.id)
        user.Fund = user.Fund if user.Fund is not None else 0
        transaction.BeforeFund = user.Fund

        if trans.TransTypeOperation == BankTransaction.TO_STORE_TRANSFER:
            # TODO: Crear una función para calcular el fund del dealer
            user.Fund -= commission.Ammount
        else:
            user.Fund = PersonController().available_fund(member=user)
            user.CountableBalance = PersonController().accounting_fund(member=user)
            transaction.BalanceFund = user.Fund

        user.save()
        transaction.save()

    @staticmethod
    def update_transaction_to_processed(tran):

        if (tran.content_type.name == 'contact'):
            user = Contact.objects.get(id=tran.object_id)
        else:
            user = Person.objects.get(id=tran.object_id)
 
        '''
        Actualiza la transacción del member
        '''
        tran.TransStatus = BankTransaction.STATUS_PROCESSED
        tran.ResponseDate = datetime.datetime.now()
        tran.save()

        # Actualiza la transaction espejo para los members
        try:
            trans_vault = BankTransaction.objects.get(offset_trans_id=tran.id,
                                                      TransTypeOperation=tran.TransTypeOperation,
                                                      TransContactType=TYPE_VAULT)
            trans_vault.TransStatus = BankTransaction.STATUS_PROCESSED
            trans_vault.ResponseDate = datetime.datetime.now()
            trans_vault.save()
        except BankTransaction.DoesNotExist:
            pass

        # Actualiza los fondos del member
        if (tran.content_type.name == 'person'):
            user.Fund = PersonController().available_fund(member=user)
            user.CountableBalance = PersonController().accounting_fund(member=user)
            user.save()
            # Actualizamos el Balance fund de la transacción. 
            if tran.TransTypeOperation == BankTransaction.TYPE_DEPOSIT:
                tran.BalanceFund = user.Fund
                tran.TransDate = datetime.datetime.now()
                tran.save()

        # Actualiza los fondos del dealer
        if (tran.content_type.name == 'contact'):
            user.Fund = ContactController().available_fund(contact=user)
            user.CountableBalance = ContactController().accounting_fund(contact=user)
            user.save()
            # Actualizamos el Balance fund de la transacción. 
            if tran.TransTypeOperation == BankTransaction.TYPE_DEPOSIT:
                tran.BalanceFund = user.Fund
                tran.TransDate = datetime.datetime.now()
                tran.save()

        
        # Para un pago se actualiza la transacción del dealer
        if tran.TransTypeOperation == BankTransaction.TO_STORE_PAYMENT or tran.TransTypeOperation == BankTransaction.TO_STORE_TRANSFER:
            try:
                tran_dealer = BankTransaction.objects.get(offset_trans_id=tran.id,
                                                          TransTypeOperation=tran.TransTypeOperation,
                                                          TransContactType=TYPE_DEALER)
                TransferController.update_transaction_to_processed(tran_dealer)
            except BankTransaction.DoesNotExist:
                pass


class SubscriptionController(object):
    @staticmethod
    def update_next_date(subscription):
        frequency = Frequency.objects.get(id=subscription.Frequency_id)
        subscription.NextDate = None

        if frequency.number_days == 0:
            subscription.Status = BankAccountSubscription.STATUS_FINISH
        else:
            current_date = subscription.StartDate
            end_date = subscription.EndDate
            start_date = current_date + datetime.timedelta(days=frequency.number_days)

            if str(start_date) > str(end_date):
                subscription.Status = BankAccountSubscription.STATUS_FINISH
                # subscription.StartDate = None
            else:
                next_date = start_date + datetime.timedelta(days=frequency.number_days)
                if str(next_date) <= str(end_date):
                    subscription.NextDate = next_date

                subscription.StartDate = start_date

        subscription.save()


class ContactController(object):
    @staticmethod
    def available_fund(contact_id=None, contact=None):

        if not contact:
            try:
                contact = Contact.objects.get(id=contact_id)
            except Contact.DoesNotExist:
                raise Exception('Failed in get data of Dealer')

        all_transactions = contact.transactions.all()
        sum_deposits = 0
        sum_transfer_debit = 0

        for transfer in all_transactions:

            # transacciones credito para el dealer
            if transfer.TransTypeOperation == BankTransaction.TO_DEPOSIT or transfer.TransTypeOperation == BankTransaction.TO_STORE_PAYMENT or transfer.TransTypeOperation == BankTransaction.TO_STORE_TRANSFER:
                if transfer.TransStatus == BankTransaction.STATUS_PROCESSED:
                    sum_deposits += transfer.TransCredit

            # retiros que realiza el dealer de sus fondos a sus cuentas bancarias
            if transfer.TransTypeOperation == BankTransaction.TO_MY_BANK or transfer.TransTypeOperation == BankTransaction.TO_STORE:
                if transfer.TransStatus == BankTransaction.STATUS_PROCESSED or transfer.TransStatus == BankTransaction.STATUS_PENDING:
                    sum_transfer_debit += transfer.TransDebit

        return sum_deposits - sum_transfer_debit

    @staticmethod
    def accounting_fund(contact_id=None, contact=None):

        if not contact:
            try:
                contact = Contact.objects.get(id=contact_id)
            except Contact.DoesNotExist:
                raise Exception('Failed in get data of Dealer')

        all_transactions = contact.transactions.all()
        sum_deposits = 0
        sum_transfer_debit = 0

        for transfer in all_transactions:

            if ((transfer.TransTypeOperation == BankTransaction.TO_DEPOSIT or
                         transfer.TransTypeOperation == BankTransaction.TO_STORE_PAYMENT or
                         transfer.TransTypeOperation == BankTransaction.TO_STORE_TRANSFER) and
                        transfer.TransStatus == BankTransaction.STATUS_PROCESSED):
                sum_deposits += transfer.TransCredit

            if ((transfer.TransTypeOperation == BankTransaction.TO_MY_BANK or
                         transfer.TransTypeOperation == BankTransaction.TO_STORE) and
                        transfer.TransStatus == BankTransaction.STATUS_PROCESSED):
                sum_transfer_debit += transfer.TransDebit

        return sum_deposits - sum_transfer_debit
