import React, { Component } from 'react';
import {
  Button,
  Header,
  Icon,
  Navigation,
  Spacer,
  Textfield,
  Card,
  CardActions,
  CardMenu,
  CardTitle,
  CardText,
  IconButton,
  FABButton,
} from 'react-mdl';

import DataTable from '../../common/components/DataTable';
import SelectField from '../../common/components/SelectField';
import Error from '../../common/components/Error';
import Csrf from '../../common/components/Csrf';
import { process } from '../../utils';

const TextFieldMonto = ({ item, index, onChangeMonto }) => (
  <div>
    <Textfield
      type="number"
      name="montos"
      value={item.monto_a_pagar}
      onChange={(e) => onChangeMonto(index, e.target.value)}
      min="1"
      max={item.raw_deuda}
      label=""
      pattern="[0-9]*(\.[0-9]+)?"
      required
      floatingLabel
    />
    <input type="hidden" name="items" value={item.id} />
  </div>
);

const ButtonRemove = ({
  index,
  onRemoveClick,
}) => (
  <IconButton
    title="Remover"
    type="button"
    onClick={() => onRemoveClick(index)}
    name="close" />
);

const initialState = {
  alumno: '',
  items: [],
  item: null,
};

class ItemsBazar extends Component {
  state = initialState;

  onChangeAlumno = (e) => {
    const { url_items } = this.props;
    const alumno = e.target.value;

    this.setState({
      alumno,
      items: [],
    });

    process({
      url: `${url_items}?id=${alumno}`,
      method: 'GET',
      onSuccess: this.onSuccess,
      onError: this.onError,
    });
  };

  onChangeItem = (e) => {
    const { items } = this.state;
    const id = e.target.value;
    const item = items.filter(item => item.id == id).pop() || null;

    this.setState({ item });
  };

  onSuccess = (data) => {
    console.log(data);

    this.setState({
      items: data.items,
    });
  };

  onError = (a, b) => {
    window.showError('No se pudieron obtener los items');
  };

  onDoneClick = () => {
    const {
      alumno,
      item,
    } = this.state;

    if (alumno && item) {
      this.props.onDoneClick(item);
      this.setState(initialState);
    }
  };

  onCancelClick = () => {
    this.setState(initialState);
    this.props.onCancelClick();
  };

  getRowNotAdding = () => ({
    key: 'row_not_adding',
    checkbox: (
      <FABButton mini primary
        key="add"
        title="Agregar item"
        type="button"
        onClick={this.props.onAddClick}>
        <Icon name="add" />
      </FABButton>
    ),
  });

  getRowAdding = () => {
    const { alumnos } = this.props;
    const {
      alumno,
      items,
      item,
    } = this.state;

    const buttons = (
      <span>
        <FABButton mini primary
          key="done"
          title="Aceptar"
          type="button"
          onClick={this.onDoneClick}>
          <Icon name="done" />
        </FABButton>
        <IconButton
          key="cancel"
          title="Cancelar"
          type="button"
          onClick={this.onCancelClick}
          name="close" />
      </span>
    );
    const selectAlumno = (
      <SelectField
        label="Alumno"
        onChange={this.onChangeAlumno}
        value={alumno}
        floatingLabel>
        <option value=""></option>
        {alumnos.map(alumno => (
          <option key={alumno.id} value={alumno.id}>{`${alumno.grado} - ${alumno.nombre}`}</option>
        ))}
      </SelectField>
    );
    const selectItems = (
      <SelectField
        label="Item"
        onChange={this.onChangeItem}
        floatingLabel>
        <option value=""></option>
        {items.map(item => (
          <option key={item.id} value={item.id}>{item.item_nombre}</option>
        ))}
      </SelectField>
    );

    return {
      key: 'row_adding',
      checkbox: buttons,
      alumno: selectAlumno,
      item_nombre: selectItems,
      deuda: item ? item.deuda : '',
    };
  };

  render() {
    const {
      alumno_items,
      onAddClick,
      onCancelClick,
      onRemoveClick,
      onChangeMonto,
      is_adding,
    } = this.props;

    const columns = [
      {name: 'checkbox'},
      {name: 'alumno', label: 'Alumno'},
      {name: 'item_nombre', label: 'Item'},
      {name: 'deuda', label: 'Deuda'},
      {name: 'monto_a_pagar', label: 'Monto a pagar (S/.)'},
      {name: 'actions'},
    ];

    const rows = alumno_items.map((item, i) => ({
      checkbox: <ButtonRemove index={i} onRemoveClick={onRemoveClick} />,
      ...item,
      key: item.id,
      monto_a_pagar: <TextFieldMonto index={i} item={item} onChangeMonto={onChangeMonto} />
    }));

    if ( ! is_adding) {
      rows.push(this.getRowNotAdding());
    } else {
      rows.push(this.getRowAdding());
    }

    return (
      <div className="box-content">
        <h4 className="mdl-color-text--grey">Bazar</h4>
        <DataTable className="table table-pagos_new"
          theadCN="mdl-color--grey-300"
          shadow={2}
          headers={columns}
          rows={rows}
        />
      </div>
    );
  }
}

export default ItemsBazar;
