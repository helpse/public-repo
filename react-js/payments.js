import React, { Component } from 'react';
import {
  Button,
  Header,
  Icon,
  Navigation,
  Spacer,
  Textfield,
  Card,
  CardActions,
  CardMenu,
  CardTitle,
  CardText,
  IconButton,
} from 'react-mdl';

import SelectField from '../../common/components/SelectField';
import DataTable from '../../common/components/DataTable';
import Error from '../../common/components/Error';
import Csrf from '../../common/components/Csrf';

const Title = ({ boleta }) => (
  <div className="page-title">
    <h4 className="title mdl-color-text--grey">{boleta.name}</h4>
  </div>
);

const get_input = ({
  field,
  boleta,
  onChange
}) => (
  <Textfield
    key={field.name}
    name={field.name}
    label={field.label}
    disabled={ ! field.editing}
    pattern={field.pattern}
    error={field.error}
    required={field.required !== false}
    value={boleta[field.name]}
    onChange={(e) => onChange(e, field)}
    floatingLabel
  />
);

const get_select = ({
  field,
  boleta,
  onChange
}) => (
  <SelectField
    key={field.name}
    name={field.name}
    label={field.label}
    value={(field.choices.filter(choice => (choice.value == boleta[field.name])).pop() || {}).key}
    onChange={(e) => onChange(e, field)}
    required={field.required}
    floatingLabel
  >
    <option value=""></option>
    {field.choices.map(choice => (
      <option key={choice.key} value={choice.key}>{choice.value}</option>
    ))}
  </SelectField>
);

const get_field = (args) => (
  args.field.choices && args.field.editing ? get_select(args) : get_input(args)
);

class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
      boleta: { ...props.boleta }, // COPYING
      editing: false,
    };
  }

  onEditClick = () => {
    this.setState({ editing: true });
  };

  onChangeField = (e, field) => {
    const boleta = this.state.boleta;
    boleta[field.name] = e.target.value;

    this.setState({ boleta });
  };

  onCancelClick = () => {
    this.setState({
      boleta: { ...this.props.boleta },
      editing: false,
    });
  };

  /*onSaveClick = () => {

  };*/

  render () {
    //console.log(alumno);

    const {
      boleta,
      editing,
    } = this.state;

    const {
      formas_pago
    } = this.props;

    const fields = [
      {
        label: 'Estado',
        name: 'estado',
      },
      {
        label: 'Tipo',
        name: 'tipo',
      },
      {
        label: 'N° serie',
        name: 'serie',
        editing,
      },
      {
        label: 'Número',
        name: 'numero',
        editing,
      },
      {
        label: 'Forma de pago',
        name: 'forma_pago',
        choices: formas_pago,
        required: true,
        editing,
      },
      {
        label: 'Dirección',
        name: 'direccion',
        required: false,
        editing,
      },
      {
        label: 'Fecha generado',
        name: 'fecha_generado',
        pattern: '\\d{2}/\\d{2}/\\d{4}',
        editing,
      },
      {
        label: 'Fecha pagado',
        name: 'fecha_pagado',
        pattern: '\\d{2}/\\d{2}/\\d{4}',
        editing,
      },
      {
        label: 'Subtotal',
        name: 'subtotal',
      },
      {
        label: 'IGV',
        name: 'igv',
      },
      {
        label: 'Total',
        name: 'total',
      },
    ];

    const textFields = fields.map((field) => get_field({ onChange: this.onChangeField, field, boleta }));
      /*<Textfield
        key={field.name}
        name={field.name}
        label={field.label}
        disabled={ ! field.editing}
        pattern={field.pattern}
        error={field.error}
        required={field.required !== false}
        value={boleta[field.name]}
        onChange={(e) => this.onChangeField(e, field)}
        floatingLabel
      />
    ));*/

    return (
      <form className="matricula_alumno" method="post">
        <Card shadow={0}>
          <CardTitle>Datos</CardTitle>
          <CardText>
            {textFields}
          </CardText>
          <CardMenu>
            { editing ?
              <div>
                <IconButton name="done" title="Guardar" type="submit" key="save" />
                <IconButton name="clear" title="Cancelar" onClick={this.onCancelClick} key="cancel" />
              </div> :
              <IconButton name="edit" title="Editar" type="button" onClick={this.onEditClick} key="edit" /> }
          </CardMenu>
        </Card>
        <Csrf />
      </form>
    );
  }
}

const ItemsServicio = ({
  items_servicio,
}) => {
  const columns = [
    {name: 'servicio_name', label: 'Concepto'},
    {name: 'alumno', label: 'Alumno'},
    {name: 'grado', label: 'Grado'},
    {name: 'monto_pagado', label: 'Pagado'},
    {name: 'actions'},
  ];

  const rows = items_servicio.map((servicio, i) => ({
    ...servicio,
    alumno: <a href={servicio.alumno_url}>{servicio.alumno}</a>,
    grado: <a href={servicio.grado_url}>{servicio.grado}</a>,
  }));

  return (
    <div className="box-content">
      <h4 className="mdl-color-text--grey">Servicios educativos</h4>
      <DataTable className="table table-matricula_alumno"
        theadCN="mdl-color--grey-300"
        shadow={2}
        headers={columns}
        rows={rows}
      />
    </div>
  );
};

const ItemsBazar = ({
  items_bazar,
}) => {
  const columns = [
    {name: 'item_name', label: 'Item'},
    {name: 'alumno', label: 'Alumno'},
    {name: 'grado', label: 'Grado'},
    {name: 'monto_pagado', label: 'Pagado'},
    {name: 'actions'},
  ];

  const rows = items_bazar.map((servicio, i) => ({
    ...servicio,
    alumno: <a href={servicio.alumno_url}>{servicio.alumno}</a>,
    grado: <a href={servicio.grado_url}>{servicio.grado}</a>,
  }));

  return (
    <div className="box-content">
      <h4 className="mdl-color-text--grey">Servicios educativos</h4>
      <DataTable className="table table-matricula_alumno"
        theadCN="mdl-color--grey-300"
        shadow={2}
        headers={columns}
        rows={rows}
      />
    </div>
  );
};

class PagosBoleta extends Component {
  render() {
    const {
      boleta,
      formas_pago,
      items_servicio,
      items_bazar,
      ...urls,
    } = window.__CONTEXT__;

    return (
      <div>
        <Title boleta={boleta} />
        <div className="box mdl-color--white mdl-shadow--4dp">
          <Form boleta={boleta} formas_pago={formas_pago} />

          {items_servicio.length ? <ItemsServicio items_servicio={items_servicio} /> : ''}

          {items_bazar.length ? <ItemsBazar items_bazar={items_bazar} /> : ''}

          {/*<Actions {...urls} />
          <Table servicios={servicios} />*/}
        </div>
        <Error />
      </div>
    );
  }
}

export default PagosBoleta;
